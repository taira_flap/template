gulp = require 'gulp';
# sass = require 'gulp-sass'
# jade = require 'gulp-jade'
# coffee = require 'gulp-coffee'
# plumber = require 'gulp-plumber'
# autoprefixer = require 'gulp-autoprefixer'
# browser = require 'browser-sync'
runSequence = require 'run-sequence'
# clean = require 'gulp-clean'
# imagemin = require 'gulp-imagemin'
# pngquant = require 'imagemin-pngquant'
# csscomb = require 'gulp-csscomb'
# watch = require 'gulp-watch'
connect = require 'gulp-connect'

dir = {
    src: '_src',
    dist: 'dist'
}

gulp.task 'connect', ->
  console.log 'test'
  connect.server({
      port: 3000,
      root: dir.src,
      livereload: true
  });

# gulp.task("sass", function() {
#     gulp.src("sass/**/*scss")
#         .pipe(sass())
#         .pipe(autoprefixer())
#         .pipe(gulp.dest("./css"));
# });

# gulp.task('connect', function() {
#     connect.server({
#         port: 3000, // ポート番号を設定
#         root: dir.src, // ルートディレクトリの場所を指定
#         livereload: true // ライブリロードを有効にする
#     });
# });

gulp.task 'default', () ->
  runSequence(
    'connect'
  )
